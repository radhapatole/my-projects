import nltk
from nltk import sent_tokenize
from nltk import word_tokenize
from nltk.probability import FreqDist
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import nltk.data
import itertools
import string

a_file = open('C:\\Users\\Radha\\OneDrive - Drexel University\\College\\Senior Design\\CI 491 Senior Project I\\Natural Language Processing\\NICE.csv')

desc = []
key = []
desc2 = []
descend = []
end = []

#separate each sentence into words
for line in a_file:
  stripped_line2 = line.replace('"', '')
  stripped_line = stripped_line2.replace(',', '')
  line_list = stripped_line.split()
  desc.append(line_list)

#currently, the keys are connected to the first word
#E.G 'K2034Knowledge'
#separate the keys by taking the first 5 characters and appending it to the list named key
for s in desc:
    key.append(s[0][:5])
    s.append('end')

#replace the first word of each sentence, that includes the key, with the word 'Knowledge'
for word in desc:
    word[0] = 'Knowledge'

#function for picking out patterns of words that that are relevant from the descriptions
def match(tagged):
    #define parts of speech identifiers
    JJ = 'JJ'
    NN = 'NN'
    NNS = 'NNS'
    VBG = 'VBG'
    NNP = 'NNP'
    VB = 'VB'
    IN = 'IN'

    for s in tagged:
        end = len(s)
        for ind, (a, b) in enumerate (s, 1):
            if ind == end:
                break
            if b == NN or b == NNS or b == VBG:
                var = "{}".format(a) #NN or NNS or VBG; if not in any other pair
                if b == NN:
                    if s[ind - 2][1] == JJ and s[ind][1] == NNS:
                        var = "{} {} {}".format(s[ind-2][0], a, s[ind][0]) #JJ NN NNS
                    elif s[ind - 2][1] == VBG:
                        var = "{} {}".format(s[ind-2][0], a) #VBG NN
                    elif s[ind - 2][1] == JJ:
                        var = "{} {}".format(s[ind-2][0], a) #JJ NN
                    elif s[ind][1] == NNS:
                        var = "{} {}".format(a, s[ind][0]) #NN NNS
                    elif s[ind - 2][1] == NNP:
                        var = "{} {}".format(s[ind-2][0], a) #NNP NN
                    elif s[ind][1] == NN:
                        if ind != end-1:
                            if s[ind + 1][1] != NN and s[ind - 2][1] != NN:
                                var = "{} {}".format(a, s[ind][0]) #NN NN
                            elif s[ind + 1][1] == NN:
                                var = "{} {} {}".format(a, s[ind][0], s[ind + 1][0]) #NN NN NN
                elif b == NNS:
                    if s[ind - 2][1] == VBG:
                        var = "{} {}".format(s[ind-2][0], a) #VBG NNS
                    elif s[ind - 2][1] == NNP:
                        var = "{} {}".format(s[ind-2][0], a) #NNP NNS
                    elif s[ind - 2][1] == JJ:
                        if s[ind - 3][1] == VB:
                            var = "{} {} {}".format(s[ind-3][0], s[ind-2][0], a) #VB JJ NNS
                        else:
                            var = "{} {}".format(s[ind-2][0], a) #JJ NNS
                if (b == NN and s[ind - 2][1] != NN) or (b == NNS and s[ind - 2][1] != NN) or (b == VBG and s[ind - 2][1] != NN) or (b == VBG and s[ind - 2][1] != NNS):
                    yield (var)

#tag all the words in the description
#run match on the descriptions
#append it to the list named descend
for ele in desc:
    tagged = nltk.pos_tag(ele)
    for i in range(0, 1):
        a = [tagged]
        result = list(match(a))
        descend.append(result)


#remove duplicates
for l in descend:
    list(l for l,_ in itertools.groupby(l))

#clean the words
# importing a string of punctuation and digits to remove
exclist = string.punctuation + string.digits
# remove punctuations and digits from ele
table_ = str.maketrans(exclist, ' '*len(exclist))
#new = ele.replace(ele, ' '.join(ele.translate(table_).split()))
descend = [[ele.replace(ele, ' '.join(ele.translate(table_).split())) for ele in word] for word in descend]

#connect the keys with the descriptions stored in descend
#print final result
end = list(zip(key, descend))
for x in end:
    print(x)



"""
What we're pulling in:
    JJ NN NNS
    JJ NN
    NN NNS
    NNP NN
    NN NN
    NN NN NN
    VBG NNS
    VBG NN
    NNP NNS
    VB JJ NNS

    NEW:
    NN (as long as it isn't present in another pair)
    NNS (as long as it isn't present in another pair)
    JJ NNS
"""
