import nltk
from nltk import sent_tokenize
from nltk import word_tokenize
from nltk.probability import FreqDist
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import nltk.data
import itertools
import string

a_file = open("CT.txt", "r")

#terms that we are not interested in
ignore = ['College/Department', 'Repeat Status', 'Corequisite', 'Prerequisites', '>>>']

list_of_lists = []
list_of_lists2 = []
#separate each sentence into words
for line in a_file:
  stripped_line = line.strip()
  list_of_lists2.append(stripped_line)
  line_list = stripped_line.split()
  list_of_lists.append(line_list)

a_file.close()

ans = []
#remove the terms that we are not interested in
#Listed above in the list named ignore
ans.append([sa for sa in list_of_lists2 if not any(sb in sa for sb in ignore)])

description = []
course = []
desmod = []
coumod = []

#separate the descriptions from the rest of the text
for x in ans:
    for index, element in enumerate(x):
        if index % 2 != 0:
		          description.append(element)

#separate the course names from the rest of the text
for x in ans:
    for index, element in enumerate(x):
        if index % 2 == 0:
		          course.append(element)

#remove the unwanted information from the end of the course name
#E.G  3.0 Credits
for word in course:
    coumod.append(word[:-12])

#function for picking out patterns of words that that are relevant from the descriptions
def match(tagged):
    #define parts of speech identifiers
    JJ = 'JJ'
    NN = 'NN'
    NNS = 'NNS'
    VBG = 'VBG'
    NNP = 'NNP'
    VB = 'VB'
    IN = 'IN'

    for s in tagged:
        end = len(s)
        for ind, (a, b) in enumerate (s, 1):
            if ind == end:
                break
            if b == NN or b == NNS:
                var = "{}".format(a) #NN or NNS; if not in any other pair
                if b == NN:
                    if s[ind - 2][1] == JJ and s[ind][1] == NNS:
                        var = "{} {} {}".format(s[ind-2][0], a, s[ind][0]) #JJ NN NNS
                    elif s[ind - 2][1] == JJ:
                        var = "{} {}".format(s[ind-2][0], a) #JJ NN
                    elif s[ind][1] == NNS:
                        var = "{} {}".format(a, s[ind][0]) #NN NNS
                    elif s[ind - 2][1] == NNP:
                        var = "{} {}".format(s[ind-2][0], a) #NNP NN
                    elif s[ind][1] == NN:
                        if ind != end-1:
                            if s[ind + 1][1] != NN and s[ind - 2][1] != NN:
                                var = "{} {}".format(a, s[ind][0]) #NN NN
                            elif s[ind + 1][1] == NN:
                                var = "{} {} {}".format(a, s[ind][0], s[ind + 1][0]) #NN NN NN
                elif b == NNS:
                    if s[ind - 2][1] == VBG:
                        var = "{} {}".format(s[ind-2][0], a) #VBG NNS
                    elif s[ind - 2][1] == NNP:
                        var = "{} {}".format(s[ind-2][0], a) #NNP NNS
                    elif s[ind - 2][1] == JJ:
                        if s[ind - 3][1] == VB:
                            var = "{} {} {}".format(s[ind-3][0], s[ind-2][0], a) #VB JJ NNS
                        else:
                            var = "{} {}".format(s[ind-2][0], a) #JJ NNS
                if (b == NN and s[ind - 2][1] != NN) or (b == NNS and s[ind - 2][1] != NN):
                    yield (var)

#tag all the words in the description
#run match on the descriptions
#append it to the list named desmod
for ele in description:
    mod = ele.split()
    tagged = nltk.pos_tag(mod)
    for i in range(0, 1):
        a = [tagged]
        result = list(match(a))
        desmod.append(result)

#remove duplicates
for l in desmod:
    list(l for l,_ in itertools.groupby(l))

#clean the words
# importing a string of punctuation and digits to remove
exclist = string.punctuation + string.digits
# remove punctuations and digits from ele
table_ = str.maketrans(exclist, ' '*len(exclist))
#new = ele.replace(ele, ' '.join(ele.translate(table_).split()))
desmod = [[ele.replace(ele, ' '.join(ele.translate(table_).split())) for ele in word] for word in desmod]

#connect the keys with the descriptions stored in descend
#print final result
end = list(zip(coumod, desmod))
for s in end:
    print(s)


"""
What we're pulling in:
    JJ NN NNS
    JJ NN
    NN NNS
    NNP NN
    NN NN
    NN NN NN
    VBG NNS
    NNP NNS
    VB JJ NNS

    NEW:
    NN (as long as it isn't present in another pair)
    NNS (as long as it isn't present in another pair)
    JJ NNS
"""
