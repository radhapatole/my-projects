import nltk
from nltk import sent_tokenize
from nltk import word_tokenize
from nltk.probability import FreqDist
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

#Open the text file:
text_file = open("file.txt")

text = text_file.read()

#Tokenize words
token = word_tokenize(text)
for words in token:
    tagged = nltk.pos_tag(token)

#Function to pull required terms
def match(tagged, JJ, NN, NNS, VBG, NNP, VB):
    for s in tagged:
        end = len(s)
        for ind, (a, b) in enumerate (s, 1):
            if ind == end:
                break
            if b == NN:
                if s[ind - 2][1] == JJ and s[ind][1] == NNS:
                    yield ("{} {} {}".format(s[ind-2][0], a, s[ind][0]))
                elif s[ind - 2][1] == JJ:
                    yield ("{} {}".format(s[ind-2][0], a))
                elif s[ind][1] == NNS:
                    yield ("{} {}".format(a, s[ind][0]))
                elif s[ind - 2][1] == NNP:
                    yield ("{} {}".format(s[ind-2][0], a))
                elif s[ind - 2][1] == NN and s[ind][1] == NN:
                    yield ("{} {} {}".format(s[ind-2][0], a, s[ind][0]))
                elif s[ind - 2][1] == NN:
                    yield ("{} {}".format(a, s[ind - 2][0]))
            elif b == NNS:
                if s[ind - 2][1] == VBG:
                    yield ("{} {}".format(s[ind-2][0], a))
                elif s[ind - 2][1] == NNP:
                    yield ("{} {}".format(s[ind-2][0], a))
                elif s[ind - 2][1] == JJ and s[ind - 3][1] == VB:
                    yield ("{} {} {}".format(s[ind-3][0], s[ind-2][0], a))

for i in range(0, 1):
    a = [tagged]
    ans = list(match(a, 'JJ', 'NN', 'NNS', 'VBG', 'NNP', 'VB'))
    print(ans)
